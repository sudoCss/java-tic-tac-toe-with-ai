import java.util.ArrayList;
import java.util.List;

public class Game {
	public static final char PLAYER_X = 'X';

	public static final char PLAYER_O = 'O';

	private static final char DRAW = 'D';

	private static final char EMPTY_SQUARE = ' ';

	private final char[][] board;

	private final char currentState;

	public final char currentPlayer;

	public Game() {
		this.board = new char[3][3];
		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[0].length; j++) {
				this.board[i][j] = Game.EMPTY_SQUARE;
			}
		}

		this.currentPlayer = Game.PLAYER_O;
		this.currentState = this.getCurrentState();
	}

	private Game(char[][] board, char lastPlayer, int x, int y) {
		this.board = new char[board.length][board.length];
		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[0].length; j++) {
				this.board[i][j] = board[i][j];
			}
		}

		this.board[y][x] = lastPlayer;

		this.currentPlayer = lastPlayer == Game.PLAYER_O ? Game.PLAYER_X : Game.PLAYER_O;
		this.currentState = this.getCurrentState();
	}

	public boolean canSetSquare(int x, int y) {
		return x >= 0 && x < this.board.length && y >= 0 && y < this.board.length && this.board[y][x] == Game.EMPTY_SQUARE;
	}

	public Game setSquare(int x, int y) {
		if (!this.canSetSquare(x, y)) {
			return this;
		}

		return new Game(this.board, this.currentPlayer, x, y);
	}

	public List<Game> possibleMoves() {
		List<Game> moves = new ArrayList<>();

		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[0].length; j++) {
				if (this.canSetSquare(j, i)) {
					moves.add(this.setSquare(j, i));
				}
			}
		}

		return moves;
	}

	private int countNonEmpties() {
		int res = 0;

		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[0].length; j++) {
				if (this.board[i][j] != Game.EMPTY_SQUARE) {
					res++;
				}
			}
		}

		return res;
	}

	private boolean emptySquaresExists() {
		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[0].length; j++) {
				if (this.board[i][j] == Game.EMPTY_SQUARE) {
					return true;
				}
			}
		}

		return false;
	}

	private char getCurrentState() {
		// Rows
		for (int i = 0; i < this.board.length; i++) {
			for (int j = 1; j < this.board[0].length; j++) {
				if (this.board[i][j] == Game.EMPTY_SQUARE) {
					continue;
				}

				if (this.board[i][j] != this.board[i][j - 1]) {
					break;
				}

				if (j == this.board[0].length - 1 && this.board[i][j] == this.board[i][j - 1]) {
					return this.board[i][j];
				}
			}
		}

		// Cols
		for (int i = 0; i < this.board[0].length; i++) {
			for (int j = 1; j < this.board.length; j++) {
				if (this.board[j][i] == Game.EMPTY_SQUARE) {
					continue;
				}

				if (this.board[j][i] != this.board[j - 1][i]) {
					break;
				}

				if (j == this.board.length - 1 && this.board[j][i] == this.board[j - 1][i]) {
					return this.board[j][i];
				}
			}
		}

		// Main Diagonal
		char tmp = this.board[0][0];
		if (tmp != Game.EMPTY_SQUARE) {
			for (int i = 0, j = 0; ; i++, j++) {
				if (this.board[i][j] == Game.EMPTY_SQUARE || this.board[i][j] != tmp) {
					break;
				}

				if (i == this.board.length - 1 && j == this.board[0].length - 1) {
					return tmp;
				}
			}
		}

		// Secondary Diagonal
		tmp = this.board[this.board.length - 1][0];
		if (tmp != Game.EMPTY_SQUARE) {
			for (int i = this.board.length - 1, j = 0; ; i--, j++) {
				if (this.board[i][j] == Game.EMPTY_SQUARE || this.board[i][j] != tmp) {
					break;
				}

				if (i == 0 && j == this.board[0].length - 1) {
					return tmp;
				}
			}
		}

		// Check if it's a draw state
		if (!this.emptySquaresExists()) {
			return Game.DRAW;
		}

		// The game goes on
		return Game.EMPTY_SQUARE;
	}

	public boolean isFinalState() {
		return this.currentState != Game.EMPTY_SQUARE;
	}

	public String getWinner() {
		switch (this.currentState) {
			case Game.PLAYER_O:
				return "O player";
			case Game.PLAYER_X:
				return "X player";
			case Game.DRAW:
				return "No one, it's a Draw!";
			case Game.EMPTY_SQUARE:
				return "The game isn't done yet";
			default:
				return "Unknown";
		}
	}

	public void print() {
		System.out.print("    ");
		for (int i = 0; i < this.board[0].length; i++) {
			System.out.print(i + "   ");
		}
		System.out.println();
		for (int i = 0; i < this.board.length; i++) {
			System.out.print(i + " | ");
			for (int j = 0; j < this.board[0].length; j++) {
				System.out.print(this.board[i][j] + " | ");
			}
			System.out.println();
		}
		System.out.println("-------------------------------");
	}

	public int staticEvaluation(int depth) {
		if (this.currentState == Game.PLAYER_O) {
			return 20 - depth;
		} else if (this.currentState == Game.PLAYER_X) {
			return (-20) + depth;
		}
		return 0;
	}

	public Game getBestMoveForCurrentPlayerMiniMax() {
		boolean currentPlayerIsO = this.currentPlayer == Game.PLAYER_O;
		int bestScore = currentPlayerIsO ? Integer.MIN_VALUE : Integer.MAX_VALUE;
		Game bestMove = null;
		int currentDepth = this.countNonEmpties();

		for (Game move : this.possibleMoves()) {
			int score = AI.miniMax(move, currentDepth, !currentPlayerIsO);

			if (currentPlayerIsO) {
				if (score > bestScore) {
					bestScore = score;
					bestMove = move;
				}
			} else {
				if (score < bestScore) {
					bestScore = score;
					bestMove = move;
				}
			}
		}

		return bestMove;
	}

	public Game getBestMoveForCurrentPlayerAlphaBeta() {
		boolean currentPlayerIsO = this.currentPlayer == Game.PLAYER_O;
		int bestScore = currentPlayerIsO ? Integer.MIN_VALUE : Integer.MAX_VALUE;
		Game bestMove = null;
		int currentDepth = this.countNonEmpties();

		for (Game move : this.possibleMoves()) {
			int score = AI.alphaBeta(move, currentDepth, Integer.MIN_VALUE, Integer.MAX_VALUE, !currentPlayerIsO);

			if (currentPlayerIsO) {
				if (score > bestScore) {
					bestScore = score;
					bestMove = move;
				}
			} else {
				if (score < bestScore) {
					bestScore = score;
					bestMove = move;
				}
			}
		}

		return bestMove;
	}
}
