import java.util.List;

public class AI {
	public static int miniMax(Game s, int depth, boolean maximizing) {
		if (s.isFinalState()) {
			return s.staticEvaluation(depth);
		}

		if (maximizing) {
			int maxEval = Integer.MIN_VALUE;

			List<Game> moves = s.possibleMoves();
			for(Game move : moves) {
				int eval = AI.miniMax(move, depth + 1, false);
				maxEval = Math.max(maxEval, eval);
			}

			return maxEval;
		} else {
			int minEval = Integer.MAX_VALUE;

			List<Game> moves = s.possibleMoves();
			for(Game move : moves) {
				int eval = AI.miniMax(move, depth + 1, true);
				minEval = Math.min(minEval, eval);
			}

			return minEval;
		}
	}

	public static int alphaBeta(Game s, int depth,int alpha, int beta, boolean maximizing) {
		if (s.isFinalState()) {
			return s.staticEvaluation(depth);
		}

		if (maximizing) {
			int maxEval = Integer.MIN_VALUE;

			List<Game> moves = s.possibleMoves();
			for(Game move : moves) {
				int eval = AI.alphaBeta(move, depth + 1, alpha, beta, false);
				maxEval = Math.max(maxEval, eval);

				alpha = Math.max(alpha, eval);
				if (beta <= alpha) {
					break;
				}
			}

			return maxEval;
		} else {
			int minEval = Integer.MAX_VALUE;

			List<Game> moves = s.possibleMoves();
			for(Game move : moves) {
				int eval = AI.alphaBeta(move, depth + 1, alpha, beta, true);
				minEval = Math.min(minEval, eval);

				beta = Math.min(beta, eval);
				if (beta <= alpha) {
					break;
				}
			}

			return minEval;
		}
	}
}
