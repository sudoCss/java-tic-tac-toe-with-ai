import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		while (true) {
			Game state = new Game();

			System.out.println("Choose playing method:");
			System.out.println("[1] Play with a friend.");
			System.out.println("[2] Play with AI(MiniMax).");
			System.out.println("[3] Play with AI(AlphaBeta).");
			System.out.println("[4] Play AI vs AI(MiniMax).");
			System.out.println("[5] Play AI vs AI(AlphaBeta).");
			System.out.println("[6] Quit.");
			int method = scanner.nextInt();
			switch (method) {
				case 1:
					Playing.twoPlayers(state);
					break;
				case 2:
					Playing.playWithAIMiniMax(state);
					break;
				case 3:
					Playing.playWithAIAlphaBeta(state);
					break;
				case 4:
					Playing.playAIvsAIMiniMax(state);
					break;
				case 5:
					Playing.playAIvsAIAlphaBeta(state);
					break;
				case 6:
					return;
				default:
					System.out.println("Invalid input.");
					break;
			}
		}
	}
}
