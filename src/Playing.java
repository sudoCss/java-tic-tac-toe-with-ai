import java.util.Scanner;

public class Playing {
	private static final Scanner scanner = new Scanner(System.in);

	public static void twoPlayers(Game state) {
		Game current = state;

		while (!current.isFinalState()) {
			current.print();
			System.out.println(current.currentPlayer + "'s turn.");

			System.out.println("Enter x: ");
			int x = scanner.nextInt();
			System.out.println("Enter y: ");
			int y = scanner.nextInt();

			if (current.canSetSquare(x, y)) {
				current = current.setSquare(x, y);
			} else {
				System.out.println("Not valid move.");
			}
		}

		current.print();
		System.out.println("The winner is: " + current.getWinner() + ".");
	}

	public static void playWithAIMiniMax(Game state) {
		Game current = state;

		while (!current.isFinalState()) {
			current.print();
			System.out.println(current.currentPlayer + "'s turn.");

			if (current.currentPlayer == Game.PLAYER_O) {
				System.out.println("Enter x: ");
				int x = scanner.nextInt();
				System.out.println("Enter y: ");
				int y = scanner.nextInt();

				if (current.canSetSquare(x, y)) {
					current = current.setSquare(x, y);
				} else {
					System.out.println("Not valid move.");
				}
			} else {
				current = current.getBestMoveForCurrentPlayerMiniMax();
			}
		}

		current.print();
		System.out.println("The winner is: " + current.getWinner() + ".");
	}

	public static void playWithAIAlphaBeta(Game state) {
		Game current = state;

		while (!current.isFinalState()) {
			current.print();
			System.out.println(current.currentPlayer + "'s turn.");

			if (current.currentPlayer == Game.PLAYER_O) {
				System.out.println("Enter x: ");
				int x = scanner.nextInt();
				System.out.println("Enter y: ");
				int y = scanner.nextInt();

				if (current.canSetSquare(x, y)) {
					current = current.setSquare(x, y);
				} else {
					System.out.println("Not valid move.");
				}
			} else {
				current = current.getBestMoveForCurrentPlayerAlphaBeta();
			}
		}

		current.print();
		System.out.println("The winner is: " + current.getWinner() + ".");
	}

	public static void playAIvsAIMiniMax(Game state) {
		Game current = state;

		while (!current.isFinalState()) {
			current.print();
			System.out.println(current.currentPlayer + "'s turn.");

			current = current.getBestMoveForCurrentPlayerMiniMax();
		}

		current.print();
		System.out.println("The winner is: " + current.getWinner() + ".");
	}

	public static void playAIvsAIAlphaBeta(Game state) {
		Game current = state;

		while (!current.isFinalState()) {
			current.print();
			System.out.println(current.currentPlayer + "'s turn.");

			current = current.getBestMoveForCurrentPlayerAlphaBeta();
		}

		current.print();
		System.out.println("The winner is: " + current.getWinner() + ".");
	}
}
